<?php

namespace App\Entity;

use App\Repository\DeviceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DeviceRepository::class)
 * @ORM\Table(name="devices")
 */
class Device
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string 
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $serial_number;

    /**
     * @ORM\OneToMany(targetEntity=DeviceReset::class, mappedBy="device")
     */
    private $deviceResets;

    /**
     * @ORM\OneToMany(targetEntity=DeviceCalibration::class, mappedBy="device")
     */
    private $deviceCalibrations;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="device")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=DeviceCompany::class, mappedBy="device")
     */
    private $deviceCompanies;

    /**
     * @ORM\OneToMany(targetEntity=DeviceInspector::class, mappedBy="device")
     */
    private $deviceInspectors;

    public function __construct()
    {
        $this->deviceResets = new ArrayCollection();
        $this->deviceCalibrations = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->deviceCompanies = new ArrayCollection();
        $this->deviceInspectors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serial_number;
    }

    public function setSerialNumber(?string $serial_number): self
    {
        $this->serial_number = $serial_number;

        return $this;
    }

    public function getDeviceResets(): Collection
    {
        return $this->deviceResets;
    }

    
    public function getDeviceCalibrations(): Collection
    {
        return $this->deviceCalibrations;
    }

    
    public function getUsers(): Collection
    {
        return $this->users;
    }

    
    public function getDeviceCompanies(): Collection
    {
        return $this->deviceCompanies;
    }

   
    public function getDeviceInspectors(): Collection
    {
        return $this->deviceInspectors;
    }
}
