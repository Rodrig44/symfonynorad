<?php

namespace App\Repository;

use App\Entity\DeviceCompany;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DeviceCompany|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeviceCompany|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeviceCompany[]    findAll()
 * @method DeviceCompany[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceCompanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeviceCompany::class);
    }

    // /**
    //  * @return DeviceCompany[] Returns an array of DeviceCompany objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeviceCompany
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
