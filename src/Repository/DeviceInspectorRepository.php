<?php

namespace App\Repository;

use App\Entity\DeviceInspector;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DeviceInspector|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeviceInspector|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeviceInspector[]    findAll()
 * @method DeviceInspector[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceInspectorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeviceInspector::class);
    }

    // /**
    //  * @return DeviceInspector[] Returns an array of DeviceInspector objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeviceInspector
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
