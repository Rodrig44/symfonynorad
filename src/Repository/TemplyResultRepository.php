<?php

namespace App\Repository;

use App\Entity\TemplyResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TemplyResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemplyResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemplyResult[]    findAll()
 * @method TemplyResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TemplyResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TemplyResult::class);
    }

    // /**
    //  * @return TemplyResult[] Returns an array of TemplyResult objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TemplyResult
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
