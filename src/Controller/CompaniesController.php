<?php

namespace App\Controller;

use App\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompaniesController extends AbstractController
{
    /**
     * @Route("/companies", name="companies")
     */
    public function index(): Response
    {
        $companyRepository = $this->getDoctrine()->getRepository(Company::class);
        $companies = $companyRepository->findAll();

        foreach($companies as $key => $company)
        {
            $companiesArray[$key]['name'] = $company->getName();
            $companiesArray[$key]['id'] =  $company->getId();
        }

        return $this->render('companies/index.html.twig', [
            'companies' => $companiesArray
        ]);
    }
}
