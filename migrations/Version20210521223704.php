<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210521223704 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activities_log (id INT AUTO_INCREMENT NOT NULL, description LONGTEXT DEFAULT NULL, timestamp DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE agreements (id INT AUTO_INCREMENT NOT NULL, content LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, email VARCHAR(100) DEFAULT NULL, phone VARCHAR(100) DEFAULT NULL, address VARCHAR(100) DEFAULT NULL, email_2 VARCHAR(100) DEFAULT NULL, name_client VARCHAR(100) DEFAULT NULL, email_client VARCHAR(100) DEFAULT NULL, email2client VARCHAR(100) DEFAULT NULL, name_agent VARCHAR(100) DEFAULT NULL, email_agent VARCHAR(100) DEFAULT NULL, email2agent VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE companies (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, payment_type_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, address_1 VARCHAR(200) DEFAULT NULL, address_2 VARCHAR(200) DEFAULT NULL, city VARCHAR(200) DEFAULT NULL, state VARCHAR(200) DEFAULT NULL, zip VARCHAR(10) DEFAULT NULL, phone VARCHAR(100) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, contact VARCHAR(100) DEFAULT NULL, status TINYINT(1) NOT NULL, customer_profile_id INT DEFAULT NULL, payment_profile_id INT DEFAULT NULL, test_fee DOUBLE PRECISION DEFAULT NULL, INDEX IDX_8244AA3AA76ED395 (user_id), INDEX IDX_8244AA3ADC058279 (payment_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE companies_agreements (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, agreement_id INT DEFAULT NULL, date DATETIME DEFAULT NULL, accepted DATETIME DEFAULT NULL, INDEX IDX_A2B7E7D4979B1AD6 (company_id), INDEX IDX_A2B7E7D424890B2B (agreement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE companies_fee (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, fee_from INT DEFAULT NULL, fee_to INT DEFAULT NULL, fee DOUBLE PRECISION DEFAULT NULL, INDEX IDX_7F8D531979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devices (id INT AUTO_INCREMENT NOT NULL, serial_number VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devices_calibration (id INT AUTO_INCREMENT NOT NULL, device_id INT NOT NULL, date_time DATETIME NOT NULL, constant DOUBLE PRECISION NOT NULL, INDEX IDX_F9CE133294A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devices_companies (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, device_id INT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME DEFAULT NULL, INDEX IDX_C46D55F1979B1AD6 (company_id), INDEX IDX_C46D55F194A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devices_inspectors (id INT AUTO_INCREMENT NOT NULL, inspector_id INT NOT NULL, device_id INT NOT NULL, company_id INT DEFAULT NULL, start_date DATETIME NOT NULL, end_date DATETIME DEFAULT NULL, INDEX IDX_6DE67A35D0E3F35F (inspector_id), INDEX IDX_6DE67A3594A4C7D4 (device_id), INDEX IDX_6DE67A35979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devices_reset (id INT AUTO_INCREMENT NOT NULL, device_id INT NOT NULL, date_time DATETIME NOT NULL, INDEX IDX_1A8712294A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inspectors (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, user_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, certification_number VARCHAR(50) DEFAULT NULL, status TINYINT(1) NOT NULL, email VARCHAR(100) DEFAULT NULL, INDEX IDX_611648F2979B1AD6 (company_id), INDEX IDX_611648F2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pass_forgotten_request (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, time DATETIME DEFAULT NULL, passkey VARCHAR(400) DEFAULT NULL, INDEX IDX_8B48EC18A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment_details (id INT AUTO_INCREMENT NOT NULL, payment_id INT DEFAULT NULL, des VARCHAR(255) DEFAULT NULL, subtotal DOUBLE PRECISION DEFAULT NULL, quantity INT DEFAULT NULL, INDEX IDX_6B6F05604C3A3BB (payment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payments (id INT AUTO_INCREMENT NOT NULL, payment_type_id INT DEFAULT NULL, date DATETIME DEFAULT NULL, total DOUBLE PRECISION DEFAULT NULL, is_played TINYINT(1) DEFAULT NULL, number INT DEFAULT NULL, INDEX IDX_65D29B32DC058279 (payment_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payments_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE temply_results (id INT AUTO_INCREMENT NOT NULL, test_id INT NOT NULL, date_time DATETIME NOT NULL, alpha DOUBLE PRECISION DEFAULT NULL, radon DOUBLE PRECISION DEFAULT NULL, temperature DOUBLE PRECISION DEFAULT NULL, humidity DOUBLE PRECISION DEFAULT NULL, pressure DOUBLE PRECISION DEFAULT NULL, error TINYINT(1) DEFAULT NULL, tilt TINYINT(1) DEFAULT NULL, power_outage TINYINT(1) DEFAULT NULL, INDEX IDX_606958B11E5D0459 (test_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tests (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, device_inspector_id INT DEFAULT NULL, payment_id INT DEFAULT NULL, address VARCHAR(100) NOT NULL, addres_2 VARCHAR(100) DEFAULT NULL, city VARCHAR(100) NOT NULL, state VARCHAR(100) NOT NULL, zip VARCHAR(10) NOT NULL, foor_level VARCHAR(30) NOT NULL, room VARCHAR(30) NOT NULL, comments_begin LONGTEXT DEFAULT NULL, comments_end LONGTEXT DEFAULT NULL, report_date DATETIME DEFAULT NULL, file VARCHAR(200) DEFAULT NULL, image_1 VARCHAR(200) DEFAULT NULL, image_2 VARCHAR(200) DEFAULT NULL, image_3 VARCHAR(300) DEFAULT NULL, image_description_1 VARCHAR(200) DEFAULT NULL, image_description_2 VARCHAR(200) DEFAULT NULL, image_description_3 VARCHAR(200) DEFAULT NULL, auth_code VARCHAR(6) DEFAULT NULL, transaction_id INT DEFAULT NULL, receipt_id INT DEFAULT NULL, INDEX IDX_1260FC5E19EB6921 (client_id), INDEX IDX_1260FC5EA296148E (device_inspector_id), INDEX IDX_1260FC5E4C3A3BB (payment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, device_id INT DEFAULT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(50) NOT NULL, UNIQUE INDEX UNIQ_1483A5E9F85E0677 (username), INDEX IDX_1483A5E994A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE companies ADD CONSTRAINT FK_8244AA3AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE companies ADD CONSTRAINT FK_8244AA3ADC058279 FOREIGN KEY (payment_type_id) REFERENCES payments_type (id)');
        $this->addSql('ALTER TABLE companies_agreements ADD CONSTRAINT FK_A2B7E7D4979B1AD6 FOREIGN KEY (company_id) REFERENCES companies (id)');
        $this->addSql('ALTER TABLE companies_agreements ADD CONSTRAINT FK_A2B7E7D424890B2B FOREIGN KEY (agreement_id) REFERENCES agreements (id)');
        $this->addSql('ALTER TABLE companies_fee ADD CONSTRAINT FK_7F8D531979B1AD6 FOREIGN KEY (company_id) REFERENCES companies (id)');
        $this->addSql('ALTER TABLE devices_calibration ADD CONSTRAINT FK_F9CE133294A4C7D4 FOREIGN KEY (device_id) REFERENCES devices (id)');
        $this->addSql('ALTER TABLE devices_companies ADD CONSTRAINT FK_C46D55F1979B1AD6 FOREIGN KEY (company_id) REFERENCES companies (id)');
        $this->addSql('ALTER TABLE devices_companies ADD CONSTRAINT FK_C46D55F194A4C7D4 FOREIGN KEY (device_id) REFERENCES devices (id)');
        $this->addSql('ALTER TABLE devices_inspectors ADD CONSTRAINT FK_6DE67A35D0E3F35F FOREIGN KEY (inspector_id) REFERENCES inspectors (id)');
        $this->addSql('ALTER TABLE devices_inspectors ADD CONSTRAINT FK_6DE67A3594A4C7D4 FOREIGN KEY (device_id) REFERENCES devices (id)');
        $this->addSql('ALTER TABLE devices_inspectors ADD CONSTRAINT FK_6DE67A35979B1AD6 FOREIGN KEY (company_id) REFERENCES companies (id)');
        $this->addSql('ALTER TABLE devices_reset ADD CONSTRAINT FK_1A8712294A4C7D4 FOREIGN KEY (device_id) REFERENCES devices (id)');
        $this->addSql('ALTER TABLE inspectors ADD CONSTRAINT FK_611648F2979B1AD6 FOREIGN KEY (company_id) REFERENCES companies (id)');
        $this->addSql('ALTER TABLE inspectors ADD CONSTRAINT FK_611648F2A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE pass_forgotten_request ADD CONSTRAINT FK_8B48EC18A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE payment_details ADD CONSTRAINT FK_6B6F05604C3A3BB FOREIGN KEY (payment_id) REFERENCES payments (id)');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT FK_65D29B32DC058279 FOREIGN KEY (payment_type_id) REFERENCES payments_type (id)');
        $this->addSql('ALTER TABLE temply_results ADD CONSTRAINT FK_606958B11E5D0459 FOREIGN KEY (test_id) REFERENCES tests (id)');
        $this->addSql('ALTER TABLE tests ADD CONSTRAINT FK_1260FC5E19EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE tests ADD CONSTRAINT FK_1260FC5EA296148E FOREIGN KEY (device_inspector_id) REFERENCES devices_inspectors (id)');
        $this->addSql('ALTER TABLE tests ADD CONSTRAINT FK_1260FC5E4C3A3BB FOREIGN KEY (payment_id) REFERENCES payments (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E994A4C7D4 FOREIGN KEY (device_id) REFERENCES devices (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE companies_agreements DROP FOREIGN KEY FK_A2B7E7D424890B2B');
        $this->addSql('ALTER TABLE tests DROP FOREIGN KEY FK_1260FC5E19EB6921');
        $this->addSql('ALTER TABLE companies_agreements DROP FOREIGN KEY FK_A2B7E7D4979B1AD6');
        $this->addSql('ALTER TABLE companies_fee DROP FOREIGN KEY FK_7F8D531979B1AD6');
        $this->addSql('ALTER TABLE devices_companies DROP FOREIGN KEY FK_C46D55F1979B1AD6');
        $this->addSql('ALTER TABLE devices_inspectors DROP FOREIGN KEY FK_6DE67A35979B1AD6');
        $this->addSql('ALTER TABLE inspectors DROP FOREIGN KEY FK_611648F2979B1AD6');
        $this->addSql('ALTER TABLE devices_calibration DROP FOREIGN KEY FK_F9CE133294A4C7D4');
        $this->addSql('ALTER TABLE devices_companies DROP FOREIGN KEY FK_C46D55F194A4C7D4');
        $this->addSql('ALTER TABLE devices_inspectors DROP FOREIGN KEY FK_6DE67A3594A4C7D4');
        $this->addSql('ALTER TABLE devices_reset DROP FOREIGN KEY FK_1A8712294A4C7D4');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E994A4C7D4');
        $this->addSql('ALTER TABLE tests DROP FOREIGN KEY FK_1260FC5EA296148E');
        $this->addSql('ALTER TABLE devices_inspectors DROP FOREIGN KEY FK_6DE67A35D0E3F35F');
        $this->addSql('ALTER TABLE payment_details DROP FOREIGN KEY FK_6B6F05604C3A3BB');
        $this->addSql('ALTER TABLE tests DROP FOREIGN KEY FK_1260FC5E4C3A3BB');
        $this->addSql('ALTER TABLE companies DROP FOREIGN KEY FK_8244AA3ADC058279');
        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY FK_65D29B32DC058279');
        $this->addSql('ALTER TABLE temply_results DROP FOREIGN KEY FK_606958B11E5D0459');
        $this->addSql('ALTER TABLE companies DROP FOREIGN KEY FK_8244AA3AA76ED395');
        $this->addSql('ALTER TABLE inspectors DROP FOREIGN KEY FK_611648F2A76ED395');
        $this->addSql('ALTER TABLE pass_forgotten_request DROP FOREIGN KEY FK_8B48EC18A76ED395');
        $this->addSql('DROP TABLE activities_log');
        $this->addSql('DROP TABLE agreements');
        $this->addSql('DROP TABLE clients');
        $this->addSql('DROP TABLE companies');
        $this->addSql('DROP TABLE companies_agreements');
        $this->addSql('DROP TABLE companies_fee');
        $this->addSql('DROP TABLE devices');
        $this->addSql('DROP TABLE devices_calibration');
        $this->addSql('DROP TABLE devices_companies');
        $this->addSql('DROP TABLE devices_inspectors');
        $this->addSql('DROP TABLE devices_reset');
        $this->addSql('DROP TABLE inspectors');
        $this->addSql('DROP TABLE pass_forgotten_request');
        $this->addSql('DROP TABLE payment_details');
        $this->addSql('DROP TABLE payments');
        $this->addSql('DROP TABLE payments_type');
        $this->addSql('DROP TABLE temply_results');
        $this->addSql('DROP TABLE tests');
        $this->addSql('DROP TABLE users');
    }
}
