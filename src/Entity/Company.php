<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 * @ORM\Table(name="companies")
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="companies")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $address_1;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $address_2;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $contact;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $customer_profile_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $payment_profile_id;

    /**
     * @ORM\ManyToOne(targetEntity=PaymentType::class, inversedBy="companies")
     */
    private $payment_type;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $test_fee;

    /**
     * @ORM\OneToMany(targetEntity=CompanyAgreement::class, mappedBy="company")
     */
    private $companyAgreements;

    /**
     * @ORM\OneToMany(targetEntity=CompanyFee::class, mappedBy="company")
     */
    private $companyFees;

    /**
     * @ORM\OneToMany(targetEntity=DeviceCompany::class, mappedBy="company")
     */
    private $deviceCompanies;

    /**
     * @ORM\OneToMany(targetEntity=Inspector::class, mappedBy="company")
     */
    private $inspectors;

    /**
     * @ORM\OneToMany(targetEntity=DeviceInspector::class, mappedBy="company")
     */
    private $deviceInspectors;

    public function __construct()
    {
        $this->companyAgreements = new ArrayCollection();
        $this->companyFees = new ArrayCollection();
        $this->deviceCompanies = new ArrayCollection();
        $this->inspectors = new ArrayCollection();
        $this->deviceInspectors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address_1;
    }

    public function setAddress1(?string $address_1): self
    {
        $this->address_1 = $address_1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address_2;
    }

    public function setAddress2(?string $address_2): self
    {
        $this->address_2 = $address_2;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCustomerProfileId(): ?int
    {
        return $this->customer_profile_id;
    }

    public function setCustomerProfileId(?int $customer_profile_id): self
    {
        $this->customer_profile_id = $customer_profile_id;

        return $this;
    }

    public function getPaymentProfileId(): ?int
    {
        return $this->payment_profile_id;
    }

    public function setPaymentProfileId(?int $payment_profile_id): self
    {
        $this->payment_profile_id = $payment_profile_id;

        return $this;
    }

    public function getPaymentType(): ?PaymentType
    {
        return $this->payment_type;
    }

    public function setPaymentType(?PaymentType $payment_type): self
    {
        $this->payment_type = $payment_type;

        return $this;
    }

    public function getTestFee(): ?float
    {
        return $this->test_fee;
    }

    public function setTestFee(?float $test_fee): self
    {
        $this->test_fee = $test_fee;

        return $this;
    }

    public function getCompanyAgreements(): Collection
    {
        return $this->companyAgreements;
    }

    public function getCompanyFees(): Collection
    {
        return $this->companyFees;
    }

   
    public function getDeviceCompanies(): Collection
    {
        return $this->deviceCompanies;
    }

    public function getInspectors(): Collection
    {
        return $this->inspectors;
    }

    public function getDeviceInspectors(): Collection
    {
        return $this->deviceInspectors;
    }
}
