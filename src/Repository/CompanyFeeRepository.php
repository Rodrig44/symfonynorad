<?php

namespace App\Repository;

use App\Entity\CompanyFee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CompanyFee|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyFee|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyFee[]    findAll()
 * @method CompanyFee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyFeeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyFee::class);
    }

    // /**
    //  * @return CompanyFee[] Returns an array of CompanyFee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompanyFee
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
