<?php

namespace App\Repository;

use App\Entity\PassForgottenRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PassForgottenRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method PassForgottenRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method PassForgottenRequest[]    findAll()
 * @method PassForgottenRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PassForgottenRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PassForgottenRequest::class);
    }

    // /**
    //  * @return PassForgottenRequest[] Returns an array of PassForgottenRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PassForgottenRequest
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
