<?php

namespace App\Controller;

use App\Entity\Device;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {

        $deviceRepository = $this->getDoctrine()->getRepository(Device::class);
        $devices = $deviceRepository->findAll();

        foreach ($devices as $key => $device){
            $showDevices[$key]['serialNumber'] = $device->getSerialNumber();
            if(!empty($device->getDeviceCompanies())){
                foreach($device->getDeviceCompanies() as $company){
                    if(empty($company->getEndDate())){
                        $showDevices[$key]['companyUsed'] = $company->getCompany()->getName();
                    }
                }
                foreach($device->getDeviceInspectors() as $inspector){
                    if(empty($inspector->getEndDate())){
                        $showDevices[$key]['inspectorUsed'] = $inspector->getInspector()->getName();
                    }
                }
            }
            if(empty($showDevices[$key]['companyUsed'])){
                $showDevices[$key]['companyUsed'] = 'Not Assigned';
            } 
            if(empty($showDevices[$key]['inspectorUsed'])){
                $showDevices[$key]['inspectorUsed'] = 'Not Assigned';
            } 
        }
        return $this->render('admin/index.html.twig', [
            'devices' => $showDevices
        ]);
    }
}
