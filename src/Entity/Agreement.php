<?php

namespace App\Entity;

use App\Repository\AgreementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AgreementRepository::class)
 * @ORM\Table(name="agreements")
 */
class Agreement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\OneToMany(targetEntity=CompanyAgreement::class, mappedBy="agreement")
     */
    private $companyAgreements;

    public function __construct()
    {
        $this->companyAgreements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|CompanyAgreement[]
     */
    public function getCompanyAgreements(): Collection
    {
        return $this->companyAgreements;
    }

    public function addCompanyAgreement(CompanyAgreement $companyAgreement): self
    {
        if (!$this->companyAgreements->contains($companyAgreement)) {
            $this->companyAgreements[] = $companyAgreement;
            $companyAgreement->setAgreement($this);
        }

        return $this;
    }

    public function removeCompanyAgreement(CompanyAgreement $companyAgreement): self
    {
        if ($this->companyAgreements->removeElement($companyAgreement)) {
            // set the owning side to null (unless already changed)
            if ($companyAgreement->getAgreement() === $this) {
                $companyAgreement->setAgreement(null);
            }
        }

        return $this;
    }
}
