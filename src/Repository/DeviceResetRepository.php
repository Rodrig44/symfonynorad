<?php

namespace App\Repository;

use App\Entity\DeviceReset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DeviceReset|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeviceReset|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeviceReset[]    findAll()
 * @method DeviceReset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceResetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeviceReset::class);
    }

    // /**
    //  * @return DeviceReset[] Returns an array of DeviceReset objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeviceReset
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
