<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InspectorsController extends AbstractController
{
    /**
     * @Route("/inspectors", name="inspectors")
     */
    public function index(): Response
    {
        return $this->render('inspectors/index.html.twig', [
            'controller_name' => 'InspectorsController',
        ]);
    }
}
