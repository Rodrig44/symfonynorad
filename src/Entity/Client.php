<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 * @ORM\Table(name="clients")
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email_2;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nameClient;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $emailClient;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email2Client;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nameAgent;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $emailAgent;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email2Agent;

    /**
     * @ORM\OneToMany(targetEntity=Test::class, mappedBy="client")
     */
    private $tests;

    public function __construct()
    {
        $this->tests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail2(): ?string
    {
        return $this->email_2;
    }

    public function setEmail2(?string $email_2): self
    {
        $this->email_2 = $email_2;

        return $this;
    }

    public function getNameClient(): ?string
    {
        return $this->nameClient;
    }

    public function setNameClient(?string $nameClient): self
    {
        $this->nameClient = $nameClient;

        return $this;
    }

    public function getEmailClient(): ?string
    {
        return $this->emailClient;
    }

    public function setEmailClient(?string $emailClient): self
    {
        $this->emailClient = $emailClient;

        return $this;
    }

    public function getEmail2Client(): ?string
    {
        return $this->email2Client;
    }

    public function setEmail2Client(?string $email2Client): self
    {
        $this->email2Client = $email2Client;

        return $this;
    }

    public function getNameAgent(): ?string
    {
        return $this->nameAgent;
    }

    public function setNameAgent(?string $nameAgent): self
    {
        $this->nameAgent = $nameAgent;

        return $this;
    }

    public function getEmailAgent(): ?string
    {
        return $this->emailAgent;
    }

    public function setEmailAgent(?string $emailAgent): self
    {
        $this->emailAgent = $emailAgent;

        return $this;
    }

    public function getEmail2Agent(): ?string
    {
        return $this->email2Agent;
    }

    public function setEmail2Agent(?string $email2Agent): self
    {
        $this->email2Agent = $email2Agent;

        return $this;
    }

    /**
     * @return Collection|Test[]
     */
    public function getTests(): Collection
    {
        return $this->tests;
    }

    public function addTest(Test $test): self
    {
        if (!$this->tests->contains($test)) {
            $this->tests[] = $test;
            $test->setClient($this);
        }

        return $this;
    }

    public function removeTest(Test $test): self
    {
        if ($this->tests->removeElement($test)) {
            // set the owning side to null (unless already changed)
            if ($test->getClient() === $this) {
                $test->setClient(null);
            }
        }

        return $this;
    }
}
