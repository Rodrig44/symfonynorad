<?php

namespace App\Entity;

use App\Repository\CompanyFeeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyFeeRepository::class)
 * @ORM\Table(name="companies_fee")
 */
class CompanyFee
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="companyFees")
     */
    private $company;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fee_from;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fee_to;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $fee;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getFeeFrom(): ?int
    {
        return $this->fee_from;
    }

    public function setFeeFrom(?int $fee_from): self
    {
        $this->fee_from = $fee_from;

        return $this;
    }

    public function getFeeTo(): ?int
    {
        return $this->fee_to;
    }

    public function setFeeTo(?int $fee_to): self
    {
        $this->fee_to = $fee_to;

        return $this;
    }

    public function getFee(): ?float
    {
        return $this->fee;
    }

    public function setFee(?float $fee): self
    {
        $this->fee = $fee;

        return $this;
    }
}
